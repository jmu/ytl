/**
 * Group model
 * @param {object} data json data
 */
function Group(data) {
    this.id = ko.observable(data.group.id);
    this.email = ko.observable(data.group.email);
    // this.name = ko.observable(data.group.name);
    this.name = ko.observable(data.group.name);
    // Collect contacts
    this.contacts = ko.observableArray([]);
    for (var i = data.contacts.length - 1; i >= 0; i--) {
      this.contacts.push(new Contact(data.contacts[i]));
    }
}

/**
 * Contact model
 * @param {object} data json data from the backend
 */
function Contact(data) {
    this.key = ko.observable(data.key);
    this.id = ko.observable(data.id);
    this.groupId = ko.observable(data.group_id);
    this.firstName = ko.observable(data.firstname);
    this.lastName = ko.observable(data.lastname);
    this.email = ko.observable(data.email);
    this.comment = ko.observable(data.comment);
    this.unit = ko.observable(data.unit);
    this.street = ko.observable(data.street);
    this.zipCode = ko.observable(data.zipcode);
    this.municipality = ko.observable(data.municipality);
    this.phones = ko.observableArray(data.phones || []);

    this.zipInfo = ko.computed(function(){
      return (this.zipCode() || "") + " " + (this.municipality() || "");
    }, this);

    this.fullName = ko.computed(function(){
      return this.firstName() + " " + this.lastName();
    }, this);

    this.phonesList = ko.computed(function(){
      return this.phones().join(', ');
    }, this);

    this.removeNumber = function(index) {
      //var numbers = this.phones();
      //numbers.splice(index, 1);
      //this.phones(numbers);
      this.phones.splice(index, 1);
    };

    this.addNumber = function(element) {
      element = $(element), number = element.val();
      this.phones.push(number);
      // Empty field
      var numberField = $('input[type="text"]').filter('input[value="' + number + '"]');
      element.val("");
    };
}

function ChangeRequest(data) {
  var self = this;

  // Inherit value
  var contact = new Contact(data);
  $.each(contact, function(name, value){
    self[name] = value;
  });

  self.type = ko.observable(data.type);
  //self.contactId = ko.observable(data.contact_id);
  self.contact = null;
  if (data.contact !== null) {
    self.contact = new Contact(data.contact);
  }

  self.typeName = {1: 'update', 2:'create', 0:'delete'};
}

/**
 * Object for holding contact lists
 */
function ContactListModel() {
  // Data
  var self = this;

  self.addContactToggled = ko.observable(false);
  self.newContact = ko.observable();
  self.contactGroups = ko.observableArray([]);
  self.contactsOnly = ko.observableArray([]);
  self.messages = ko.observableArray([]);
  self.changeRequests = ko.observableArray([]);
  self.changeRequestIds = ko.computed(function(){
    var ids = [];
    $.each(self.changeRequests(), function(key, request){
      if (request.contact !== null){
        ids.push(request.contact.id());
      }
    });
    return ids;
  });
  self.newTaskText = ko.observable();
  self.currentContact = ko.observable();
  self.currentGroup = ko.observable();
  self.isAdmin = ko.observable(false);

  self.inEdit = function(contact) {
    return self.currentContact && contact.key == self.currentContact.key;
  };

  self.isChangeRequestPending = function(contact) {
    if (typeof contact !== 'undefined') {
      return $.inArray(contact.id(), self.changeRequestIds()) !== -1;
    }
    return false;
  };

  /**
   * Resets the newContact object and sets the addContactToggled info
   * @return {none}
   */
  self.toggleAddContact = function(state, reset) {
    if (typeof reset === 'undefined') {
      reset = false;
    }
    if (typeof state === 'undefined') {
      state = !self.addContactToggled();
    };
    if (reset) {
      self.newContact(new Contact({}));
    }
    self.addContactToggled(state);
  };

  self.noop = function(contact) {
    // No nothing
    return false;
  };

  // Operations
  self.addContact = function() {
    self.contactsOnly.push(new Contact({
      title: this.newTaskText()
    }));
    self.newTaskText("");
  };

  self.removeContact = function(contact) {
    self.contactsOnly.remove(contact);
  };

  self.editContact = function(contact) {
    window.console.log('Edit contact: ' + contact.fullName());
    self.currentContact(contact);
    self.currentGroup(self.getGroup(contact.groupId()));
  };

  /**
   * Copying the given contact as new
   * @param  {object} contact to copy
   */
  self.copyContact = function(contact) {
    window.console.log('Copy contact: ' + contact.fullName());

    var preFill = {
      firstname: contact.firstName(),
      lastname: contact.lastName(),
      comment: contact.comment(),
      phones: contact.phones(),
      street: contact.street(),
      zipcode: contact.zipCode(),
      municipality: contact.municipality(),
      unit: contact.unit()
    }

    self.newContact(new Contact(preFill));
    self.addContactToggled(true);
    self.currentGroup(self.getGroup(contact.groupId()));
  };

  self.cancelContactEdit = function() {
    // Reset current selection and load data from server
    self.currentContact(null);
    self.load();
  };

  self.cancelAllActions = function(data, event) {
    // If not esc button, continue
    if (event.keyCode != 27) {
      return true;
    }
    self.cancelContactEdit();
    self.toggleAddContact(false);
    return false;
  }

  /**
   * returns contact group based on given id
   * @param  {int} gid Group id
   * @return {object} Group
   */
  self.getGroup = function (gid) {
    var group = undefined;
    gid = parseInt(gid);
    $.each(self.contactGroups(), function(index, cGroup) {
      if (cGroup.id() === gid) {
        group = cGroup;
        return false;
      }
    });
    return group;
  }

  /**
   * Send change request for the contact
   * @param type
   */
  self.sendChangeRequest = function(type) {
    var selectContact = {create: self.newContact(), update: this, 'delete': this};
    var contact = selectContact[type];
    contact.groupId = self.currentGroup().id;

    // Post change request to backend
    $.ajax(window.ytl.apiURL + "/request" , {
      type: 'POST',
      data: {contact: ko.toJSON(contact), type: type},
      beforeSend: function() {
        $.blockUI();
      },
      success: function(){
        $.blockUI({
          message: '<div class="alert alert-info">Muutospyyntö vastaanotettu. Huomaa, että muutokset näkyvät vasta hyväskynnän jälkeen</div>',
          onOverlayClick: $.unblockUI
        });
        self.load();
        setTimeout($.unblockUI, 5000);
      },
      error: function(){
        // TODO: Replace with nice error report
        var message = '<div class="alert alert-error"><h4>Auts!</h4><p>Muutospyynnön lähettäminen epäonnistui.Ole hyvä ja tarkista sisältö tai yritä myöhemmin uudelleen (jos kyseessä onkin ohjelmistovirhe)</p></div>';
        $.blockUI({
          'message': message,
          onOverlayClick: $.unblockUI
        });
        setTimeout($.unblockUI, 5000);
      }
    });

    // Scroll up the edit mode
    $('form[name="new"]').slideUp('fast', function(){
      self.addContactToggled(false);
      self.currentContact(null);
    });

  };

  self.onKeyPress = function(contact, event) {
    window.console.log(event);
  };

  /**
   * Load status from backendwarning
   */
  self.load = function(callback) {
    callback = callback || function(){};
    // Set global variables into observables
    self.isAdmin(window.ytl.isAdmin);

    // Load initial state from server, convert it to Task instances, then populate self.tasks
    $.getJSON(window.ytl.apiURL + "/request/list", function(json) {

      $.each(json, function(key, cr) {
        var mappedRequests = $.map(json, function(request) { return new ChangeRequest(request); });
        self.changeRequests(mappedRequests);
        // self.changeRequests.push(cr.contact_id);
      });

      // After changerequests, retrieve contactsOnly
      $.getJSON(window.ytl.apiURL + "/contact/list", function(json) {
          var mappedContactGroups = $.map(json, function(contactGroup) {
            return new Group(contactGroup);
          });
          self.contactGroups(mappedContactGroups);
          callback();
      });

    });

    // Load messages
    $.getJSON(window.ytl.apiURL + "/message/list", function(messages){
      self.messages(messages);
    });
  };

  self.saveContact = function(contact) {
    // Reset current contact
    self.currentContact(null);

    $.ajax(window.ytl.apiURL + "/contact/" + contact.id(), {
      type: 'POST',
      data: {contact: ko.toJSON(contact)}
    });
  };

  self.resolveRequest = function(action) {
    var request = this;

    // Post to backend
    $.ajax(window.ytl.apiURL + "/request/" + request.id() + '/' + action, {
      type: 'POST',
      success: function() {
        // Remove request from changeRequests array
        var filteredArray = $.grep(self.changeRequests(), function(element){
          return element.id() != request.id();
        });
        self.changeRequests(filteredArray);

        // Load info from backend
        $.blockUI();
        self.load(function(){
          $.unblockUI();
        });
      }
    });
  };

}


/**
 * Load contact list information from be at page load
 * and apply to Knockout
 */
$(document).ready(function(){
  var clist = new ContactListModel();
  clist.load();
  window.ko.applyBindings(clist);

  // Some defaults to overlays
  $.blockUI.defaults.message = '<span class="icon-spin icon-spinner icon-2x"></span>';
  $.blockUI.defaults.css.padding = 10;
  $.blockUI.defaults.css.margin = 10;

});
