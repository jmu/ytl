# -*- coding: utf-8 -*-
__author__ = 'juha'

import logging

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import mail


class Group(db.Model):
  created = db.DateTimeProperty(auto_now_add=True)
  name = db.StringProperty(required=True)
  email = db.EmailProperty(required=True)

  def __str__(self):
    return self.name

  def __json__(self):
    return {
      'id': self.key().id(),
      'key': str(self.key()),
      'name': self.name,
      'email': self.email
    }


class Account(db.Model):
  """Account logged into service"""
  user = db.UserProperty(required=True)
  is_admin = db.BooleanProperty(default=False)


class Contact(db.Model):
  group = db.ReferenceProperty(Group)
  substituted_by = db.SelfReferenceProperty()
  unit = db.StringProperty()
  firstname = db.StringProperty()
  lastname = db.StringProperty()
  street = db.PostalAddressProperty()
  zipcode = db.StringProperty()
  municipality = db.StringProperty()
  phones = db.ListProperty(str)
  email = db.EmailProperty()
  areas = db.ListProperty(str)
  created = db.DateTimeProperty(auto_now_add=True)
  comment = db.StringProperty(multiline=True)
  available = db.BooleanProperty(default=True)

  def __str__(self):
    return '%s %s (id: %s, group: %s)' % (self.firstname, self.lastname, self.key().id(), self.group)

  def __json__(self):
    return {
      'id': self.key().id(),
      'key': str(self.key()),
      'firstname': self.firstname,
      'lastname': self.lastname,
      'email': self.email,
      'comment': self.comment,
      'unit': self.unit,
      'street': self.street,
      'zipcode': self.zipcode,
      'municipality': self.municipality,
      'phones': self.phones,
      'group_id': str(self.group.key().id()) if self.group else None,
      'group': self.group or None
    }


class ChangeRequest(Contact):
  TYPE_CREATE = 2
  TYPE_UPDATE = 1
  TYPE_DELETE = 0
  contact = db.ReferenceProperty(Contact)
  requested_by = db.StringProperty()
  requested = db.DateTimeProperty()
  type = db.IntegerProperty(choices=(TYPE_CREATE, TYPE_UPDATE, TYPE_DELETE), required=True)
  note = db.StringProperty()

  contact_fields = ('group', 'unit', 'firstname', 'lastname', 'street', 'zipcode', 'municipality', 'phones', 'email', 'areas', 'comment')

  def __str__(self):
    return u'%s: %s' % (self.key(), self.contact)

  def __json__(self):
    contact = super(ChangeRequest, self).__json__()
    contact.update({
      'id': self.key().id(),
      'key': str(self.key()),
      'requested': self.requested,
      #'contact_id': self.contact.key().id() if self.contact else None,
      'type': self.type,
      'contact': Contact.get_by_id(self.contact.key().id()) if self.contact else None
    })

    return contact

  def create_contact(self):
    contact = Contact()
    for name in self.contact_fields:
      setattr(contact, name, getattr(self, name))
    contact.put()

    logging.info(u'Created new contact: {0}'.format(contact))

  def update_contact(self):
    """
    Applies the changes to contact and returns it back
    """
    contact = Contact.get_by_id(self.contact.key().id())

    for name in self.contact_fields:
      setattr(contact, name, getattr(self, name))
    contact.put()

    logging.info('Applied changes to contact: %s' % contact)

  def delete_contact(self):
    """
    Applies the changes to contact and returns it back
    """
    contact = Contact.get_by_id(self.contact.key().id())
    contact.delete()

    logging.info('Deleted contact: %s' % contact)

  def put(self):
    super(ChangeRequest, self).put()

    receivers = [account.user.email() for account in Account.all().filter('is_admin = ', True)]
    subject = '[ksptytl] Uusi muutospyyntö'
    msg = '''
    Palveluun on saapunut uusi muutospyyntö. Voit käydä hyväksymässä/hylkäämässä
    sen osoitteessa http://ksptytl.appspot.com/
    '''
    self._notify(receivers, subject, msg)

  def _notify(self, receivers, subject, msg):
    """
    Notify related participants (admins) about the ChangeRequest
    """
    for receiver in receivers:
      logging.info('Sending notification to: %s' % receiver)
      mail.send_mail(
        sender="Keski-Suomen Yhteystietolista <info@ksptytl.appspotmail.com>",
        to=receiver,
        subject=subject,
        body=msg
      )


class ChangeLog(db.Model):
  created_by = users.get_current_user()
  changed = db.DateTimeProperty(auto_now_add=True)
