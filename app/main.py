# -*- coding: utf-8 -*-
import os
import webapp2


config = {}
config['env'] = {
    'production': 'Development' not in os.getenv('SERVER_SOFTWARE', ''),
}
config['webapp2_extras.sessions'] = {
    'secret_key': 'my-super-secret-key',
}
config['webapp2_extras.i18n'] = {
    'translations_path': 'locale',
    'default_locale': 'fi_FI'
}

app = webapp2.WSGIApplication([
  webapp2.Route(r'/', handler='views.HomeHandler', name='home'),
  webapp2.Route(r'/user/login', handler='views.LoginHandler', name='login'),
  webapp2.Route(r'/user/logout', handler='views.LogoutHandler', name='logout'),
  webapp2.Route(r'/user/admin', handler='views.AdminHandler', name='admin'),
  webapp2.Route(r'/request', handler='views.ChangeRequestsHandler', name='request'),
  webapp2.Route(r'/test', handler='views.TestHandler', name='test'),
  webapp2.Route(r'/api/', name='api'),
  webapp2.Route(r'/api/message/list', handler='api.ListMessagesHandler'),
  webapp2.Route(r'/api/contact/list', handler='api.ListContactsHandler'),
  webapp2.Route(r'/api/contact/<id:\d+>', handler='api.ContactsHandler', name='api-contact'),
  webapp2.Route(r'/api/contact', handler='api.AddContactHandler', name='api-contact-add'),
  webapp2.Route(r'/api/request/list', handler='api.ListChangeRequestsHandler', name='api-request-list'),
  webapp2.Route(r'/api/request/<id:\d+>', handler='api.ChangeRequestHandler', name='api-request', defaults={'id': None}),
  webapp2.Route(r'/api/request/<id:\d+>/<action>', handler='api.ResolveChangeRequestHandler', name='api-request-resolve', defaults={'action': None}),
  webapp2.Route(r'/api/request', handler='api.AddChangeRequestHandler', name='api-request-add'),
  ], debug=True, config=config)



