# -*- coding: utf-8 -*-
"""
Modules contains the views
"""
__author__ = 'juha'

import logging

import webapp2
from webapp2_extras.jinja2 import get_jinja2
from webapp2_extras.i18n import _
from webapp2 import uri_for

from google.appengine.api import users

from db import Contact, Group, Account


class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        return get_jinja2(app=self.app)

    def render_response(self, _template, **context):
        # Renders a template and writes the result to the response.
        current_user = users.get_current_user()

        # If user is logged in, ensure we have account if
        if current_user:
          account = Account.all().filter('user =', current_user).get()
          if not account:
            account = Account(user = current_user)
            account.put()
            logging.info('Registered new account: %s' % current_user.nickname())

          # Set/update admin flag
          is_admin = users.is_current_user_admin()
          if account.is_admin != is_admin:
            account.is_admin = is_admin
            account.put()
            logging.info('Updated admin info for: %s' % current_user.nickname())

        default_context = {
          'uri_for': uri_for,
          'user': users.get_current_user(),
          'admin': users.is_current_user_admin(),
          '_': _,
          'route': self.request.route.name,
          'groups': Group.all()
        }
        default_context.update(context)

        rv = self.jinja2.render_template(_template, **default_context)
        self.response.write(rv)


class HomeHandler(BaseHandler):
  def get(self):
    #template = jinja_env.get_template('base.html')
    #self.response.out.write(template.render(name='John Doe', title='Testing'))
    data = {'name': 'John!'}
    return self.render_response('contacts.html', **data)


class LoginHandler(BaseHandler):
  def get(self):
    login_url = users.create_login_url(uri_for('home'))
    return self.redirect(login_url)


class LogoutHandler(BaseHandler):
  def get(self):
    logout_url = users.create_logout_url(uri_for('home'))
    return self.redirect(logout_url)


class AdminHandler(BaseHandler):
  def get(self):
    # TODO: Read app id?
    admin_url = '//appengine.google.com/dashboard?&app_id=ksptytl'
    if not self.app.config['env']['production']:
      admin_url = '/_ah/admin'

    return self.redirect(admin_url)


class ChangeRequestsHandler(BaseHandler):
  def get(self):
    return self.render_response('requests.html')


class TestHandler(BaseHandler):
  def get(self):
    logging.info('test')

    for g in ('group1', 'group2'):
      group = Group(name=g, email='%s@ksshp.fi' % g)
      group.put()

      for i in range(5):
        c = Contact(group=group, firstname='First', lastname='last', email='first.last@company.com', comment='this is a comment')
        c.put()

        logging.info('Created user')

    return self.redirect_to(_name='home')
