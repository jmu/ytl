# -*- coding: utf-8 -*-
"""
Module contains handlers for API requests
"""
__author__ = 'juha'

import logging
import webapp2
import json
from webapp2_extras.appengine.users import admin_required
from webapp2_extras import sessions
from webapp2_extras.i18n import _
from google.appengine.api import users

from db import Contact, Group, ChangeRequest


class ComplexEncoder(json.JSONEncoder):
  def default(self, obj):
    if hasattr(obj, '__json__'):
      return obj.__json__()
    return json.JSONEncoder.default(self, obj)


def jsonize(obj):
  if hasattr(obj, '__json__'):
    return obj.__json__()
  return obj


class JsonHandler(webapp2.RequestHandler):

  def dispatch(self):
    # Get a session store for this request.
    self.session_store = sessions.get_store(request=self.request)

    try:
      # Dispatch the request.
      webapp2.RequestHandler.dispatch(self)
    finally:
      # Save all sessions.
      self.session_store.save_sessions(self.response)

  @webapp2.cached_property
  def session(self):
    # Returns a session using the default cookie key.
    return self.session_store.get_session()

  def writejson(self, data='', status=200):
    self.response.headers['Content-Type'] = 'application/json'
    self.response.status = status
    self.response.out.write(json.dumps(data, cls=ComplexEncoder))


class AddContactHandler(JsonHandler):

  def post(self):
    if not users.is_current_user_admin():
      return self.abort(403)

    c = Contact(firstname='foo', lastname='bar', email='foo@domain.com')
    c.put()

    return self.writejson('OK')


class ContactsHandler(JsonHandler):
  """
  Class handles the GET and POST requests for updating the contact

  - get: Return single contact info in JSON
  - post: Update existing contact from JSON (requires admin)

  """

  # Mapping: attribute = JSON field
  field_map = {
    'unit': 'unit',
    'street': 'street',
    'zipcode': 'zipCode',
    'email': 'email',
    'comment': 'comment',
    'municipality': 'municipality',
    'firstname': 'firstName',
    'lastname': 'lastName',
    'phones': 'phones',
  }

  def get(self, id):
    raise NotImplementedError

  def post(self, id):
    """
    Update existing contact
    """
    if not users.is_current_user_admin():
      return self.abort(403)

    c = Contact.get_by_id(long(id))
    if c:

      try:
        data = json.loads(self.request.get('contact'))
        logging.info(data)
        for attr, field in self.field_map.items():
          setattr(c, attr, data.get(field, None))
        c.put()

      except ValueError:
        logging.error('Invalid data')
        return self.abort(403)

      return self.writejson('OK')

    return self.abort(404)


class ListContactsHandler(JsonHandler):
  """
  Lists all the contacts per contact groups in JSON format
  """
  def get(self):
    # Fetch all groups and contants for the groups
    groups = Group.all().order('name')

    group_contacts = []
    for group in groups:
      group_contacts.append({'group': group, 'contacts': list(Contact.all().filter('group =', group))})

    return self.writejson(list(group_contacts))


class ListMessagesHandler(JsonHandler):
  """
  Returns session messages as list.
  To write session messages in python::

    self.session.add_flash('message')

  """
  def get(self):
    messages = [msg for msg, cat in self.session.get_flashes()]
    return self.writejson(messages)


class ListChangeRequestsHandler(JsonHandler):
  """
  Handles the change requests
  """

  def get(self):
    """
    Fetch all groups and contants for the groups
    """
    requests = ChangeRequest.all()
    return self.writejson(list(requests))


class ChangeRequestHandler(JsonHandler):
  def get(self, id):
    """
    """
    cr = ChangeRequest.get_by_id(long(id))
    if cr:
      return self.writejson(cr)
    return self.abort(404)


class ResolveChangeRequestHandler(JsonHandler):
  def post(self, id, action):
    if not users.is_current_user_admin():
      return self.abort(403)

    cr = ChangeRequest.get_by_id(long(id))
    if not cr:
      return self.abort(404)

    if action == 'accept':
      if cr.type == ChangeRequest.TYPE_CREATE:
        cr.create_contact()
      elif cr.type == ChangeRequest.TYPE_UPDATE:
        cr.update_contact()
      elif cr.type == ChangeRequest.TYPE_DELETE:
        cr.delete_contact()

      # Finally, delete the change request
      self.session.add_flash(_('Changerequest #%(id)s handled', id=cr.key().id()))
      cr.delete()

    elif action == 'reject':
      cr.delete()
      return self.writejson()

    else:
      return self.abort(403)


class AddChangeRequestHandler(JsonHandler):

  types = {
    'create': ChangeRequest.TYPE_CREATE,
    'update': ChangeRequest.TYPE_UPDATE,
    'delete': ChangeRequest.TYPE_DELETE
  }

  def post(self):
    """
    Create new change request
    """
    cr = None
    field_map = ContactsHandler.field_map

    ctype = self.request.params.get('type')
    if ctype not in self.types:
      logging.error('Invalid change type')
      return self.abort(403)

    try:
      data = json.loads(self.request.POST.get('contact', ''))
      if not data:
        return self.abort(403)

      logging.info('Received changerequest:')
      logging.info(data)

      # Check group - there always needs to be one!
      g = Group.get_by_id(long(data['groupId']))
      if not g:
        return self.abort(404)

      # Update existing
      if 'id' in data:
        c = Contact.get_by_id(long(data['id']))

        # If current contact or group is not found
        if not c:
          return self.abort(404)

        # Create cr with same data as the contact, excluding key/id
        del data['key']
        del data['id']
        cr = ChangeRequest(type=self.types[ctype], contact=c)
        for attr, field in ContactsHandler.field_map.items():
          value = data.get(field, None)
          if isinstance(value, basestring) and not value:
            value = None
          if isinstance(value, list) and not value:
            value = []
          setattr(cr, attr, value)

      # Create new
      else:
        cr = ChangeRequest(type=self.types[ctype])
        for attr, field in ContactsHandler.field_map.items():
          setattr(cr, attr, data.get(field, None))

      # Update group reference and save
      cr.group = g
      cr.put()
      logging.info('Change request: %s' % cr)

      self.session.add_flash('Received change request: #%d' % cr.key().id())

    except ValueError:
      logging.exception('Invalid data')
      return self.abort(403)

    return self.writejson(cr)
