===========
YTL project
===========
This the README document for the project - to help you to get started
with the project development.

.. contents::
   :local:

Setup development environment
=============================
Run commands in root directory

- Create env::

    virtualenv --python=python2 env
    source env/bin/activate

- Install dependencies (other than coming from GAE):

    pip install pytz
    pip install babel

- Link libraries::

    ln -s env/lib/site-packages/pytz app/pytz
    ln -s env/lib/site-packages/babel app/babel


Update translations
===================
Run commands::

  pybabel extract -o ./locale/messages.pot ./
  pybabel update -l fi_FI -d ./locale -i ./locale/messages.pot
  pybabel compile -f -d ./locale

With new language first you need to initialize::

  pybabel init -l new_LANG -d ./locale -i ./locale/messages.pot


Deploy application
==================
Deploy application to Google AppEngine by running::

  appcfg update app


Setup application
=================
For time being, there are still few actions required to be done via 
admin interface after the first deployment:

- Log in with admin account
- 

